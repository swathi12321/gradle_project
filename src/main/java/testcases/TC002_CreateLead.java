package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "C002_CreateLead";
		testDescription ="Creating Lead";
		testNodes = "Leads";
		authors ="Swathi";
		category = "smoke";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public void loginLogout(String uname, String pwd,String cmpNm, String fN,String lN) {
		new LoginPage() 
		.enterUsername(uname)
		.enterPassword(pwd)  
		.clickLogin().clickCrmLink().leads().leadsTab().enterCompName(cmpNm).firstNam(fN).lastNam(lN);
	}

}











