package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class CreateLead extends ProjectMethods{

	public CreateLead() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.ID, using="createLeadForm_companyName") WebElement cmpName;
	@FindBy(how=How.ID, using="createLeadForm_firstName") WebElement firstName;
	@FindBy(how=How.ID, using="createLeadForm_lastName") WebElement lastName;
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") WebElement cLeadBtn;
	
	public CreateLead enterCompName(String data) {
		type(cmpName, data);
		return this;
	}
	
	public CreateLead firstNam(String data) {
		type(firstName, data);
		return this;
	}
	
	public CreateLead lastNam(String data) {
		type(lastName, data);
		return this;
	}
	
	public ViewLead clickCreateLead() {
		click(cLeadBtn);

		return new ViewLead();
	}
	
}







