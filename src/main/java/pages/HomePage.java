package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	public HomePage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how=How.CLASS_NAME, using="decorativeSubmit") WebElement eleLogout;
	@FindBy(how=How.XPATH,using="//a[contains(text(),'CRM/SFA')]") WebElement clickCrm;
	public LoginPage clickLogout() {
	    click(eleLogout);
	    return new LoginPage();   
	}
	public MyHome clickCrmLink() {
		click(clickCrm);
		return new MyHome();
	}
	
}







